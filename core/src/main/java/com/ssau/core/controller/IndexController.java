package com.ssau.core.controller;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class IndexController {

    @GetMapping(path = "/")
    public Object index() {
        return SecurityContextHolder.getContext().getAuthentication().getPrincipal();
    }
}
